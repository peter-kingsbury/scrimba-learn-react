# Course Notes - Learn React

https://scrimba.com/learn/learnreact/

## Why React?

* Virtual DOM
* Reusable (and clearer) Web Components
* Maintained by Facebook
* Hireable

## React DOM

* Components should be PascalCase
* They must return a single element only (though it may be nested).

```javascript
// ReactDOM.render(<h1>Hello, world!</h1>, document.getElementById("root"));

// Objective: Fill in the boilerplate React code required to render an
// unordered list (<ul>) to the page. The list should contain 3 list items
// (<li>) with anything in them you want.

// HINTS:
// import the libraries you need first
// use one of the libraries to render some JSX to the page

function MyApp() {
  return (
    <ul>
      <li>1</li>
      <li>2</li>
      <li>3</li>
    </ul>
  );
}

ReactDOM.render(<MyApp />, document.getElementById('root'));
```


## React Functional Components

```javascript
// Objectives:
// 1. Set up the basic React code from scratch
// 2. Create a functional component called MyInfo that returns the following UI:
    // a. An h1 with your name
    // b. A paragraph with a little blurb about yourself
    // c. An ordered or unordered list of the top 3 vacation spots you'd like to visit
// 3. Render an instance of that functional component to the browser
// Extra challenge: learn on your own (Google!) how you can add some style to your page.
// (We will also cover this in an upcoming lesson).

import React from 'react';
import ReactDOM from 'react-dom';

import './style.css';

function MyInfo() {
  return (
    <div>
      <h1>Peter Kingsbury</h1>
      <p>Lorem ipsum...</p>
      <ul>
        <li>Mars</li>
        <li>Venus</li>
        <li>Past the Oort Cloud</li>
      </ul>
    </div>
  );
}

ReactDOM.render(<MyInfo />, document.getElementById('root'));
```

## Move Components into Separate files

```javascript
// Component Javascript file:

import React from 'react';
function MyComponent() { /**/}
export default MyComponent;

// Another area in the program:
import React from 'react';
import ReactDOM from 'react-dom';
import MyComponent from './MyComponent';

ReactDOM.render(<MyComponent />, document.getElementById('root'));
```

## React Parent/Child Components

* elements will appear lowercase
* Components will appear PascalCase
* try to use Components where possible

```javascript
// Set up the React app from scratch
// Render an App component (defined in a separate file)
// Inside App, render:
  // 1. A Navbar component
  // 2. A MainContent component
  // 3. A Footer component

// index.js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
ReactDOM.render(<App />, document.getElementById('root'));

// components/App.js
import React from 'react';

import Nav from './Nav';
import MainContent from './MainContent';
import Footer from './Footer';

function App() {
  return (
    <div>
      <Nav />
      <MainContent />
      <Footer />
    </div>
  );
}

export default App;

// components/Nav.js
import React from 'react';

function Nav() {
  return (
    <nav>
      <p>this is nav</p>
    </nav>
  );
}

export default Nav;

// components/MainContent.js
import React from 'react';

function MainContent() {
  return (
    <main>
      <p>this is MainContent</p>
    </main>
  );
}

export default MainContent;

// components/Footer.js
import React from 'react';

function Footer() {
  return (
    <footer>
      <p>this is a footer</p>
    </footer>
  );
}

export default Footer;
```

## React Todo App - Phase 1

```javascript
// From scratch, initialize the React app
// Render an <App /> component
// Create the <App /> component from scratch
// Have the <App /> component render 3 or 4 checkboxes with paragraphs or spans next to it
// like you're making a todo list with some hard-coded items on it

// index.js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

ReactDOM.render(<App />, document.getElementById('root'));

// components/App/index.js
import React from 'react';

export default function App() {
  return (
    <div>
      <form>
        <div>
          <input type="checkbox" id="todo1" name="todo1" />
          <label for="todo1">Loaf of bread</label>
        </div>
        <div>
          <input type="checkbox" id="todo2" name="todo2" />
          <label for="todo2">Carton of milk</label>
        </div>
        <div>
          <input type="checkbox" id="todo1" name="todo1" />
          <label for="todo1">Stick of butter</label>
        </div>
      </form>
    </div>
  );
}
```

## Styling React with CSS Classes

* We cannot use `class`, instead must use `className`.
* Attribute `className` can only be applied inside React elements, but not Components.

## JSX to JavaScript and Back

* To use JavaScript functionality inside JSX markup, encapsulate 
  the JS functionality in `{curly braces}`.

```javascript
function MyComponent() {
  const nameOfWorld = 'World';
  return (
    <div>Hello, {nameOfWorld}!</div>
  );
}
```
* Mixing template literals works, too.
```javascript
function MyComponent() {
  const nameOfWorld = 'World';
  return (
    <div>`Hello, ${nameOfWorld}!`</div>
  );
}
```

## React Inline Styles with the Style Property

* Directly using traditional CSS style rules won't work:
    ```javascript
    function MyComponent() {
      return (<div style="color: #f0f0f0">This won't work</div>);
    } 
    ```
* Instead, use a braced JavaScript object to define the style properties and values:
    ```javascript
    function MyComponent() {
      return (<div style={{ backgroundColor: "#000000" }}>Hello, world!</div>);
    }
    ```
* Alternatively, you can put style properties into a variable:
    ```javascript
    function MyComponent() {
      const myStyle = { backgroundColor: 'black' };
      return (<div style={myStyle}></div>);
    }
    ```
* Some classes (prefixed with a colon) are not possible.

## React ToDo App - Phase 2

```javascript
/*
Time to have fun styling! But first things first: 

1. Change the input/p combo below to be a new component called <TodoItem />. <TodoItem /> (for now) will just have the same displayed data below (every todo item is the same) hardcoded inside of it. (We'll learn soon how to make the TodoItem more flexible)
    
2. Style up the page however you want! You're welcome to use regular CSS (in the CSS file) or inline styles, or both!
*/
```

## Properties

* You can render a list of properties simply by encapsulating them in `{}`:
  ```javascript
  <SomeComponent>
    {componentList}
  </SomeComponent>
  ```
* Each child in an array of iterator should have a unique 'key' prop.
* Use high-order functions (filter, map, sort, reduce, every, some, find, findIndex, etc) to operate on arrays for improved performance.
* Properties are _immutable_ and cannot be changed, once defined.

# Class-Based Components

* Class-based components take the following signature and minimum functionality:
  ```javascript
  import React from 'react';
  class MyComponent extends React.Component {
    render() {
      return <div></div>;
    }
  }
  ```
* In order to access `props` it must be invoked as `this.props`:

## React State

* Any component which requires statefulness, must be a Class.
* State may only be passed to child components by explicitly transmitting the properties' values via the child-component's markup properties.

## Handling Events in React

Reference: [https://reactjs.org/docs/events.html#supported-events](https://reactjs.org/docs/events.html#supported-events)

## Changing the State of a Component

* **Never** operate on a `state` object directly!
* Instead, use `this.setState(...)`!
* For convenience, always bind event-handlers to the class in that class's constructor:
  ```javascript
  class MyComponent extends Component {
    constructor(props) {
      super(props);
      this.state = {
        value: 0
      };
      this.handleClick = this.handleClick.bind(this);
    }
  
    handleClick(e) {
      // `this` refers to a class instance due to binding in the ctor.
      this.setState(prevState => {
        return {
          count: prevState.count + 1
        };
      });
    }
  }
  ```
* Always prefer using the function form of `setState()` to get access to the previous state:
  ```javascript
    // use...
    this.setState(prevState => {
      return {
        count: prevState.count + 1
      };
    });
    // ...instead of...
    this.setState({ count: this.state.count + 1 });
  ```
* *When replacing state, __always return a new object!__*

## React Lifecycle Events

* Classic: https://engineering.musefind.com/react-lifecycle-methods-how-and-when-to-use-them-2111a1b692b1
* Current: https://reactjs.org/blog/2018/03/29/react-v-16-3.html#component-lifecycle-changes

Common Useful Methods:
* `render()`: Determine what is displayed to the screen. Executed on `prop` or `state` change.
* `componentDidMount()`: Component has just been added to the DOM.
  * A good example of common usage is to perform an XHR during component startup to get server-side data.
* `componentWillReceiveProps(nextProps)`: Run on DOM-add as well as any time a parent gives new props to a child.
  * Common usage: `if (nextProps.something !== this.props.something) ...`
* `shouldComponentUpdate(nextProps, nextState)`: Gives the developer a hook to determine whether an update is required. Return `true` or `false` to trigger/not trigger refresh.
* `componentWillUnmount()`: Before removing from DOM, clean-up here.
  * Remove event-listeners or any component-specific code that needs it.











