const data = [
  {
    id: 1,
    name: 'Loaf of bread',
    completed: false
  },
  {
    id: 2,
    name: 'Carton of milk',
    completed: true
  },
  {
    id: 3,
    name: 'Stick of butter',
    completed: false
  }
];

export default data;
