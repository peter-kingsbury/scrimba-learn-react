import React, { Component } from 'react';
import './style.css';

function onClick() {
  console.log(`Click`);
}

function onMouseOver() {
  console.log(`MouseOver`);
}

function onMouseOut() {
  console.log(`MouseOut`);
}

class ImageViewer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: false
    };
  }
  render() {
    const imageSrc = `https://www.fillmurray.com/100/80`;
    const imgAlt = `Bill Murray`;
    return (
      <div>
        <img src={imageSrc} alt={imgAlt} />
        <br />
        <br />
        <button
          onClick={onClick}
          onMouseOver={onMouseOver}
          onMouseOut={onMouseOut}
        >
          Click
        </button>
      </div>
    );
  }
}

export default ImageViewer;
