const jokesData = [
  {
    punchline: 'Because!'
  },
  {
    question: 'How do you hunt a blue elephant?',
    punchline: 'With a blue elephant gun!'
  },
  {
    question: 'How do you hunt a white elephant?',
    punchline:
      'Hold his nose until he turns blue, then shoot it with a blue elephant gun!'
  }
];

export default jokesData;
