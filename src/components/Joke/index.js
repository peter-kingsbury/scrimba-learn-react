import React from 'react';
import './style.css';

export default function Joke(props) {
  const { question, punchline } = props;

  return (
    <div>
      <p className="joke-question" style={{ display: !question && 'none' }}>
        {question}
      </p>
      <p className="joke-punchline">{punchline}</p>
    </div>
  );
}
