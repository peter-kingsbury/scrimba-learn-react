import React, { Component } from 'react';

import './style.css';

class TodoItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static getDerivedStateFromProps(props) {}

  componentDidMount() {}

  componentDidUpdate() {}

  render() {
    const { id, name, completed } = this.props;
    const completedStyle = {
      //textDecoration: completed ? 'line-through' : 'none'
      fontStyle: 'italic',
      color: '#cdcdcd',
      textDecoration: 'line-through'
    };

    return (
      <div className="todoItem">
        <input
          type="checkbox"
          name={`todo_${id}`}
          onChange={() => {
            this.props.handleChange(this.props.id);
          }}
          defaultChecked={completed}
        />
        <label style={completed ? completedStyle : null} htmlFor={`todo_${id}`}>
          {name}
        </label>
      </div>
    );
  }
}

export default TodoItem;
