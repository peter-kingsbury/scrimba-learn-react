import React, { Component } from 'react';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false
    };
  }

  render() {
    return (
      <div>
        You are currently logged {this.state.isLoggedIn ? 'in' : 'out'}.
      </div>
    );
  }
}
