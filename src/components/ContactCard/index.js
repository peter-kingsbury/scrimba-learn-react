import React from 'react';
import './style.css';

export default function ContactCard({ name, imgUrl, phone, email }) {
  return (
    <div className="contact-card">
      <img src={imgUrl} alt="{name}" />
      <h4>{name}</h4>
      <p>Phone: {phone}</p>
      <a href="mailto:{email}">{email}</a>
    </div>
  );
}
