import React, { Component } from 'react';

export default class StarWarsApi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      character: null
    };
  }

  componentDidMount() {
    fetch(`https://swapi.dev/api/people/1`)
      .then(res => res.json())
      .then(data => console.log(data));
  }

  render() {
    return <div></div>;
  }
}
