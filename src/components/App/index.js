import React /*, { StrictMode }*/ from 'react';
import TodoItem from '../TodoItem';

// import ContactCard from '../ContactCard';
// import Joke from '../Joke';
// import jokesData from '../Joke/sample';

import todos from '../../data/todoItemData';
// import Login2 from '../Login2';
// import StarWarsApi from '../StarWarsApi';
// import Login from '../Login';
// import ImageViewer from '../ImageViewer';
// import StateChanger from '../StateChanger';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      todos
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(id) {
    this.setState(prevState => {
      const updatedTodos = prevState.todos.map(todo => {
        if (todo.id === id) {
          return { ...todo, completed: !todo.id };
        }
        return todo;
      });
      return {
        todos: updatedTodos
      };
    });
  }

  render() {
    const todoItems = todos.map(entry => (
      <TodoItem
        key={entry.id}
        id={entry.id}
        name={entry.name}
        completed={entry.completed}
        handleChange={this.handleChange}
      />
    ));

    return (
      <div>
        {/*<StarWarsApi />*/}
        {/*<Login2 />*/}

        {/*<StrictMode />*/}
        {/*<StateChanger />*/}
        {/*<ImageViewer />*/}
        {/*<Login />*/}
        <div className="navbar">
          <form>{todoItems}</form>
        </div>
      </div>
    );
  }
}

export default App;
