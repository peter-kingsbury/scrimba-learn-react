import React, { Component } from 'react';

export default class Login2 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loggedIn: false
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(prevState => {
      return {
        loggedIn: !prevState.loggedIn
      };
    });
  }

  render() {
    return (
      <div>
        <div>{this.state.loggedIn ? 'Logged in' : 'Logged out'}</div>
        <button onClick={this.handleClick}>
          {this.state.loggedIn ? 'Log Out' : 'Log In'}
        </button>
      </div>
    );
  }
}
